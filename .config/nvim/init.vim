"{{{Basic
"{{{BasicConfig
if empty(globpath(&runtimepath, '/autoload/plug.vim'))
  echoerr 'Unable to find autoload/plug.vim. Download it from https://github.com/junegunn/vim-plug'
  finish
endif
if !has('win32')
  set runtimepath-=/usr/share/vim/vimfiles
endif
if executable('tmux') && filereadable(expand('~/.zshrc')) && $TMUX !=# ''
  let g:vim_is_in_tmux = 1
  let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
else
  let g:vim_is_in_tmux = 0
endif
if exists('g:vim_man_pager')
  let g:vim_enable_startify = 0
else
  let g:vim_enable_startify = 1
endif
"execute 'source ' . fnamemodify(stdpath('config'), ':p') . 'env.vim'
"}}}
"FVim{{{
if exists('g:fvim_loaded')
  FVimCursorSmoothMove v:true
  FVimCursorSmoothBlink v:true
  FVimCustomTitleBar v:true
  FVimFontLigature v:true
  FVimFontNoBuiltinSymbols v:true
  FVimFontAutoSnap v:true
  FVimUIPopupMenu v:false
  FVimToggleFullScreen
endif
"}}}
"Neovide{{{
let g:neovide_cursor_vfx_mode = 'torpedo'
let g:neovide_fullscreen = v:true
"}}}
"}}}
function! s:close_on_last_tab() "{{{
  if tabpagenr('$') == 1
    execute 'windo bd'
    execute 'q'
  elseif tabpagenr('$') > 1
    execute 'windo bd'
  endif
endfunction "}}}
function! s:indent_len(str) "{{{
  return type(a:str) == 1 ? len(matchstr(a:str, '^\s*')) : 0
endfunction "}}}
function! s:go_indent(times, dir) "{{{
  for _ in range(a:times)
    let l = line('.')
    let x = line('$')
    let i = s:indent_len(getline(l))
    let e = empty(getline(l))

    while l >= 1 && l <= x
      let line = getline(l + a:dir)
      let l += a:dir
      if s:indent_len(line) != i || empty(line) != e
        break
      endif
    endwhile
    let l = min([max([1, l]), x])
    execute 'normal! '. l .'G^'
  endfor
endfunction "}}}
function! s:get_highlight() "{{{
  if !exists('*synstack')
    return
  endif
  echo map(synstack(line('.'), col('.')), 'synIDattr(v:val, "name")')
endfunc "}}}
function! s:escaped_search() range "{{{
  let l:saved_reg = @"
  execute 'normal! vgvy'
  let l:pattern = escape(@", "\\/.*'$^~[]")
  let l:pattern = substitute(l:pattern, "\n$", "", "")
  let @/ = l:pattern
  let @" = l:saved_reg
endfunction "}}}
"function! s:local_vimrc() "{{{ Apply `.settings.vim`
"  let root_dir = FindRootDirectory()
"  let settings_file = fnamemodify(root_dir, ':p') . '.settings.vim'
"  if filereadable(settings_file)
"    exec 'source ' . settings_file
"  endif
"endfunction "}}}
"}}}
"{{{Setting
set encoding=utf-8 nobomb
set fileencodings=utf-8,gbk,utf-16le,cp1252,iso-8859-15,ucs-bom
set fileformats=unix,dos,mac
scriptencoding utf-8
let g:mapleader = "\<Space>"
let g:maplocalleader = "\<A-z>"
nnoremap <SPACE> <Nop>
set number cursorline
"set cursorcolumn
set noshowmode
set incsearch
set mouse=a
filetype plugin indent on
set t_Co=256
syntax enable
set termguicolors
set smartindent
set nohlsearch
set undofile
set timeoutlen=500
set hidden
set showtabline=2
set scrolloff=5
set viminfo='1000
set autoindent
set wildmenu
set autoread
set tabstop=8 softtabstop=0 expandtab shiftwidth=4 smarttab
set signcolumn=yes
set breakindent
set updatetime=100
set history=1000
set sessionoptions+=globals
execute 'set undodir=' . fnamemodify(stdpath('cache'), ':p') . 'undo'
if !has('win32')
  set dictionary+=/usr/share/dict/words
  set dictionary+=/usr/share/dict/american-english
endif
if has('nvim')
  set inccommand=split
  set wildoptions=pum
  filetype plugin indent on
endif
augroup VimSettings
  autocmd!
  autocmd FileType html,css,scss,typescript set shiftwidth=2
  autocmd VimLeave * set guicursor=a:ver25-Cursor/lCursor
  "autocmd BufEnter * call s:local_vimrc()
augroup END
"}}}
"{{{Shortcuts
nnoremap <A-up> <C-w><up>
nnoremap <A-down> <C-w><down>
nnoremap <A-left> <C-w><left>
nnoremap <A-right> <C-w><right>


nnoremap x "dx
nnoremap d "dd
nnoremap <leader>y "+y
nnoremap <leader>p "+p
nnoremap z<left> zk
nnoremap z<right> zj
nnoremap z<up> [z
nnoremap z<down> ]z

inoremap <silent> <C-S> <Esc>:w<CR>a
"}}}
"{{{Plugin
"{{{init
"{{{ automatically install missing plugins on startup
"if g:vim_plug_auto_install == 1
"  augroup PlugAutoInstall
"    autocmd!
"    autocmd VimEnter *
"          \  if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
"          \|   PlugInstall --sync | q
"          \| endif
"  augroup END
"endif "}}}
"function! s:plug_doc() "{{{
"  let name = matchstr(getline('.'), '^- \zs\S\+\ze:')
"  if has_key(g:plugs, name)
"    for doc in split(globpath(g:plugs[name].dir, 'doc/*.txt'), '\n')
"      execute 'tabe' doc
"    endfor
"  endif
"endfunction "}}}
"function! s:plug_gx() "{{{
"  let line = getline('.')
"  let sha  = matchstr(line, '^  \X*\zs\x\{7,9}\ze ')
"  let name = empty(sha) ? matchstr(line, '^[-x+] \zs[^:]\+\ze:')
"        \ : getline(search('^- .*:$', 'bn'))[2:-2]
"  let uri  = get(get(g:plugs, name, {}), 'uri', '')
"  if uri !~# 'github.com'
"    return
"  endif
"  let repo = matchstr(uri, '[^:/]*/'.name)
"  let url  = empty(sha) ? 'https://github.com/'.repo
"        \ : printf('https://github.com/%s/commit/%s', repo, sha)
"  call netrw#BrowseX(url, 0)
"endfunction "}}}
"function! s:scroll_preview(down) "{{{
"  silent! wincmd P
"  if &previewwindow
"    execute 'normal!' a:down ? '\<c-e>' : '\<c-y>'
"    wincmd p
"  endif
"endfunction "}}}
"function! s:setup_extra_keys() "{{{
"  nnoremap <silent> <buffer> J :call <sid>scroll_preview(1)<cr>
"  nnoremap <silent> <buffer> K :call <sid>scroll_preview(0)<cr>
"  nnoremap <silent> <buffer> <c-n> :call search('^  \X*\zs\x')<cr>
"  nnoremap <silent> <buffer> <c-p> :call search('^  \X*\zs\x', 'b')<cr>
"  nmap <silent> <buffer> <c-j> <c-n>o
"  nmap <silent> <buffer> <c-k> <c-p>o
"endfunction "}}}
"augroup VimPlug
"  autocmd!
"  autocmd FileType vim-plug nmap <buffer> ? <plug>(plug-preview)
"  autocmd FileType vim-plug nnoremap <buffer> <silent> h :call <sid>plug_doc()<cr>
"  autocmd FileType vim-plug nnoremap <buffer> <silent> <Tab> :call <sid>plug_gx()<cr>
"  autocmd FileType vim-plug call s:setup_extra_keys()
"augroup END
"command PU PlugUpdate | PlugUpgrade
call plug#begin(fnamemodify(stdpath('data'), ':p') . 'plugins')
"}}}
"{{{colorschemes
Plug 'sainnhe/gruvbox-material'
Plug 'sainnhe/sonokai'
Plug 'sainnhe/everforest'
"}}}
Plug 'tpope/vim-surround'
Plug 'chaoren/vim-wordmotion'

Plug 'neovim/nvim-lspconfig'
Plug 'nvim-lua/lsp_extensions.nvim'
Plug 'nvim-lua/completion-nvim'


Plug 'simrat39/rust-tools.nvim'
Plug 'nvim-lua/popup.nvim'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'
Plug 'junegunn/vim-peekaboo'
Plug 'Yilin-Yang/vim-markbar'

Plug 'junegunn/fzf.vim'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'kevinhwang91/nvim-bqf'

Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'nvim-treesitter/nvim-treesitter-textobjects'
Plug 'RRethy/nvim-treesitter-textsubjects'

Plug 'francoiscabrol/ranger.vim'
Plug 'rbgrouleff/bclose.vim'
Plug 'tikhomirov/vim-glsl'

Plug 'scrooloose/nerdcommenter'

Plug 'kyazdani42/nvim-web-devicons' " for file icons
Plug 'kyazdani42/nvim-tree.lua'
call plug#end()



"FZF
nmap <leader><leader>f :Files<CR>
nmap <leader>b :Buffers<CR>


"{{{nvim-treesitter
lua <<EOF
require'nvim-treesitter.configs'.setup {
  ensure_installed = "maintained",
  highlight = {
    enable = true,
    disable = {"c"}
  },
  textobjects = {
    select = {
      enable = true,
      keymaps = {
        ["af"] = "@function.outer",
        ["if"] = "@function.inner",
      }
    }
  }
}
EOF
set foldmethod=expr
set foldexpr=nvim_treesitter#foldexpr()
"}}}

"{{{nerdcommenter
" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1
" Use compact syntax for prettified multi-line comments
let g:NERDCompactSexyComs = 1
" Align line-wise comment delimiters flush left instead of following code indentation
let g:NERDDefaultAlign = 'left'
" Set a language to use its alternate delimiters by default
let g:NERDAltDelims_java = 1
" Add your own custom formats or override the defaults
" let g:NERDCustomDelimiters = { 'c': { 'left': '/**','right': '*/' } }
" Allow commenting and inverting empty lines (useful when commenting a region)
let g:NERDCommentEmptyLines = 1
" Enable trimming of trailing whitespace when uncommenting
let g:NERDTrimTrailingWhitespace = 1
" Enable NERDCommenterToggle to check all selected lines is commented or not
let g:NERDToggleCheckAllLines = 1
"}}}


" Set completeopt to have a better completion experience
" :help completeopt
" menuone: popup even when there's only one match
" noinsert: Do not insert text until a selection is made
" noselect: Do not select, force user to select one from the menu
set completeopt=menuone,noinsert,noselect

" Avoid showing extra messages when using completion
set shortmess+=c

" Configure LSP
" https://github.com/neovim/nvim-lspconfig#rust_analyzer
lua <<EOF

-- nvim_lsp object
local nvim_lsp = require'lspconfig'

-- function to attach completion when setting up lsp
local on_attach = function(client)
    require'completion'.on_attach(client)
end

-- Enable rust_analyzer
nvim_lsp.rust_analyzer.setup({ on_attach=on_attach })

-- Enable diagnostics
vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(
  vim.lsp.diagnostic.on_publish_diagnostics, {
    virtual_text = true,
    signs = true,
    update_in_insert = true,
  }
)
EOF

" Use <Tab> and <S-Tab> to navigate through popup menu
inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

inoremap <silent> <c-Tab> gt
nnoremap <silent> <c-Tab> gt

" use <Tab> as trigger keys
imap <Tab> <Plug>(completion_smart_tab)
imap <S-Tab> <Plug>(completion_smart_s_tab)

" Code navigation shortcuts
nnoremap <silent> <c-]> <cmd>lua vim.lsp.buf.definition()<CR>
nnoremap <silent> K     <cmd>lua vim.lsp.buf.hover()<CR>
nnoremap <silent> gD    <cmd>lua vim.lsp.buf.implementation()<CR>
nnoremap <silent> <c-k> <cmd>lua vim.lsp.buf.signature_help()<CR>
nnoremap <silent> 1gD   <cmd>lua vim.lsp.buf.type_definition()<CR>
nnoremap <silent> gr    <cmd>lua vim.lsp.buf.references()<CR>
nnoremap <silent> g0    <cmd>lua vim.lsp.buf.document_symbol()<CR>
nnoremap <silent> gW    <cmd>lua vim.lsp.buf.workspace_symbol()<CR>
nnoremap <silent> gd    <cmd>lua vim.lsp.buf.declaration()<CR>
nnoremap <silent> ga    <cmd>lua vim.lsp.buf.code_action()<CR>

" Set updatetime for CursorHold
" 300ms of no cursor movement to trigger CursorHold
set updatetime=300
" Show diagnostic popup on cursor hold
autocmd CursorHold * lua vim.lsp.diagnostic.show_line_diagnostics()

" Goto previous/next diagnostic warning/error
nnoremap <silent> g[ <cmd>lua vim.lsp.diagnostic.goto_prev()<CR>
nnoremap <silent> g] <cmd>lua vim.lsp.diagnostic.goto_next()<CR>

" have a fixed column for the diagnostics to appear in
" this removes the jitter when warnings/errors flow in
set signcolumn=yes

" Enable type inlay hints
autocmd CursorMoved,InsertLeave,BufEnter,BufWinEnter,TabEnter,BufWritePost *
\ lua require'lsp_extensions'.inlay_hints{ prefix = '', highlight = "Comment", enabled = {"TypeHint", "ChainingHint", "ParameterHint"} }


nmap <Leader>m <Plug>ToggleMarkbar

let g:nvim_tree_lsp_diagnostics = 1
let g:nvim_tree_icons = {
    \ 'default': '',
    \ 'symlink': '',
    \ 'git': {
    \   'unstaged': "✗",
    \   'staged': "✓",
    \   'unmerged': "",
    \   'renamed': "➜",
    \   'untracked': "★",
    \   'deleted': "",
    \   'ignored': "◌"
    \   },
    \ 'folder': {
    \   'arrow_open': "",
    \   'arrow_closed': "",
    \   'default': "",
    \   'open': "",
    \   'empty': "",
    \   'empty_open': "",
    \   'symlink': "",
    \   'symlink_open': "",
    \   },
    \   'lsp': {
    \     'hint': "",
    \     'info': "",
    \     'warning': "",
    \     'error': "",
    \   }
    \ }
nnoremap <leader>F :NvimTreeToggle<CR>
nnoremap <leader>r :NvimTreeRefresh<CR>
nnoremap <leader>n :NvimTreeFindFile<CR>

"
"}}}

set grepprg=rg\ --vimgrep\ --smart-case\ --follow


"{{{colourschemes
let g:color_scheme_list = {}
let g:color_scheme_list['Gruvbox MixMat'] = [
      \   'set background=dark',
      \   "let g:gruvbox_material_background = 'soft'",
      \   "let g:gruvbox_material_palette = 'material'",
      \   "let g:gruvbox_material_visual = 'grey background'",
      \   "let g:gruvbox_material_cursor = 'green'",
      \   'let g:gruvbox_material_disable_italic_comment = 0',
      \   "let g:gruvbox_material_sign_column_background = 'none'",
      \   "let g:gruvbox_material_statusline_style = 'material'",
      \   'let g:gruvbox_material_lightline_disable_bold = 1',
      \   'let g:gruvbox_material_better_performance = 1',
      \   'colorscheme gruvbox-material'
      \   ]
let g:color_scheme_list['Gruvbox Mix Dark'] = [
      \   'set background=dark',
      \   "let g:gruvbox_material_background = 'medium'",
      \   "let g:gruvbox_material_palette = 'mix'",
      \   "let g:gruvbox_material_visual = 'grey background'",
      \   "let g:gruvbox_material_cursor = 'green'",
      \   'let g:gruvbox_material_disable_italic_comment = 1',
      \   "let g:gruvbox_material_sign_column_background = 'none'",
      \   "let g:gruvbox_material_statusline_style = 'mix'",
      \   'let g:gruvbox_material_lightline_disable_bold = 1',
      \   'let g:gruvbox_material_better_performance = 1',
      \   'colorscheme gruvbox-material'
      \   ]
"      \   'call SwitchLightlineColorScheme("gruvbox_material")'
let g:color_scheme_list['Sonokai Default'] = [
      \   "let g:sonokai_style = 'default'",
      \   'let g:sonokai_disable_italic_comment = 1',
      \   'let g:sonokai_enable_italic = 1',
      \   "let g:sonokai_cursor = 'blue'",
      \   'let g:sonokai_lightline_disable_bold = 1',
      \   "let g:sonokai_sign_column_background = 'none'",
      \   'let g:sonokai_better_performance = 1',
      \   'colorscheme sonokai'
      \   ]
"      \   'call SwitchLightlineColorScheme("sonokai")'
let g:color_scheme_list['Everforest Dark'] = [
      \   'set background=dark',
      \   'let g:everforest_disable_italic_comment = 1',
      \   "let g:everforest_sign_column_background = 'none'",
      \   'let g:everforest_lightline_disable_bold = 1',
      \   'let g:everforest_better_performance = 1',
      \   'colorscheme everforest'
      \   ]
"      \   'call SwitchLightlineColorScheme("everforest")'
function SwitchColorScheme(name) abort
  for l:item in g:color_scheme_list[a:name]
    execute l:item
  endfor
endfunction
call SwitchColorScheme('Gruvbox MixMat')
"}}}
let g:wordmotion_disable_default_mappings = v:true
nmap W <Plug>WordMotion_w
omap W <Plug>WordMotion_w
xmap W <Plug>WordMotion_w
nmap B <Plug>WordMotion_b
omap B <Plug>WordMotion_b
xmap B <Plug>WordMotion_b
nmap E <Plug>WordMotion_e
omap E <Plug>WordMotion_e
xmap E <Plug>WordMotion_e
nmap gE <Plug>WordMotion_ge
omap gE <Plug>WordMotion_ge
xmap gE <Plug>WordMotion_ge
omap aW <Plug>WordMotion_aw
xmap aW <Plug>WordMotion_aw
omap iW <Plug>WordMotion_iw
xmap iW <Plug>WordMotion_iw
